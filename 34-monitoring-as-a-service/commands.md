# 1. Intro
- Scout APM – https://scoutapm.com/blog/monitoring-docker 10$ per agent
- DataDog – https://docs.datadoghq.com/infrastructure/livecontainers/?tab=linuxwindows 15$ per agent
- Sysdig – https://sysdig.com/blog/docker-monitoring-with-datacenter/ 20$ per agent
- Sematext – https://sematext.com/blog/docker-container-monitoring-with-sematext/ price depending on data transfer