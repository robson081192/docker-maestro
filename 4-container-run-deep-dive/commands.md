## 1. Commands
```>
docker container run -d -p 8080:80 --name mynginx nginx:latest nginx -T

docker container ls

docker container logs <id_lub_nazwa_kontenera>
```

## 2. What's happening during docker container run?
1. Does the image exist locally? (if not then search it in remote repository)
2. Does the image exist in remote repository (like docker hub)? (if not return error, else download that image).
3. The container is being created and prepared for execution. 
4. The container gets an ip address in docker private network - if no network is specified there is a network left for all containers - default network.
5. The container gets a signal, that its port (80) should be forwarded to the hosts port (8080)
6. The container is starting if the instruction CMD (last instruction id Dockerfile) - it can be overriden with docker run instruction - to do that define a custom command and include it in docker run statement as last parameter)