# 1. Introduction
- connects the containers network adapter with host network adapter
- containers get an ip address from an external network
- the mac address is visible to other devices connected to the network
- containers can communicate with external providers/applications
- great use case - legacy application that requires direct access to the network
- not fully compatible with all cloud providers
- available only in docker for linux

# 2. Commands
```>
docker network create -d macvlan \
  --subnet=ADRES_TWOJEJ_PODSIECI \
  --gateway=BRAMA_DOMYŚLNA \
  -o parent=eth0 \
  mymacvlan

  docker container run -itd --name ubuntu1 --net mymacvlan ubuntu bash
```