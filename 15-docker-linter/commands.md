# Tooling
- hadolint - also available as vs code extension

# Best practices
- SINGLE RESPONSIBILITY PRINCIPLE - single container single process/application
- don't throw frontend and backend into single container
- don't add readme or config files to docker image
- use .dockerignore file
- use official docker images
- Microsoft has it's own repository (mcr)
- don't use 'latest' tag
- copy files into docker image carefully (.dockerignore)
- order of commands in DOckerfile matters! - Docker caching mechanism
- use docker linters