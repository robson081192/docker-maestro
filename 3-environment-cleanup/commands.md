## 1. How stop/remove single container
```>
nerdctl container ls

-- stop a container
nerdctl container stop {container_name_or_container_id}

-- remove a container (must be stopped)
nerdctl container rm {container_name_or_container_id}

-- stop & remove a container
nerdctl container rm --force {container_name_or_container_id}
or
nerdctl container rm -f {container_name_or_container_id}

-- return list of container id's
nerdctl container ls -q

-- stop provided container collection
nerdctl container stop $(nerdctl container ls -q)

-- remove provided container collection
nerdctl container rm --force $(nerdctl container ls -aq)
```