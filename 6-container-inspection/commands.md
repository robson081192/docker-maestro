## 1. Container inspection - useful commands
```>
docker container inspect {container_name_or_id}

docker container logs {container_name_or_id}

docker container stats {container_name_or_id}
```

## 2. Exercises for inspect
```>
nerdctl container run -it --name myalpine1 alpine:latest sh
nerdctl container inspect myalpine1

nerdctl container inspect --format='{{.Image}}' myalpine1

nerdctl container inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' myalpine1
```

## 3. Exercises for logs
```>
-- display logs
nerdctl container logs -f mynginx1 

-- display logs live
nerdctl container logs -f mynginx1 
```

## 4. Exercises for stats
```>
-- display stats (streaming)
docker container stats mynginx1

-- display stats (without streaming - free the console)
docker container stats --no-stream mynginx1
```