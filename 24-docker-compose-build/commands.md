# 1. Introduction
- docker compose makes it possible to build an image based on dockerfile. The build is triggered automatically, if the image does not exist, during:
```>
docker compose up
```

- if the image exists it's possible to rebuild it using following command:
```>
docker compose build 
or
docker compose up --build
```

# 2. Display docker-compose config
```>
docker compose config
```