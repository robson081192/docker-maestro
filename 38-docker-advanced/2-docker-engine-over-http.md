# 1. Intro
By default the Docker CLI is communicating with Docker Deamon over Unix Sockets (<code>/var/run/docker.sock</code>) and requires permissions for the "docker" group.

It's possible to communicate over TCP (by default this type of communication is not encrypted and without authorization)!!!

# 2. Demo
Demo has been performed on Ubuntu 18.04!

- first create a docker configuration file (<code>/etc/docker/daemon.json</code>) and add the following content:
```>
{
    "hosts":["unix:///var/run/docker.sock","tcp://0.0.0.0:2375"]
}
```

- second, create the file <code>/etc/systemd/system/docker.service.d/docker.conf</code> and add following content:
```>
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd
```
- restart Docker
```>
sudo systemctl restart docker
```

- communicate over http
```>
docker -H tcp://0.0.0.0:2375
```

- how prepare http requests:
```>
https://docs.docker.com/engine/api/v1.40/#operation/ContainerList
```