# 1. Intro
## 1.1. Container PID mode
- by default the container processes are isolated from host processes.

- you can allow to get access from a container to the host processes:
```
--pid=host
```
- you can also allow to get access (from a container) to the processes of a different container
```
--pid=container:<name|id>
```

## 1.2. Container network mode
- by default the container network interface is isolated from host interface and also other container network interfaces

- you can grant access to the host network interface for a container:
```
--net=host
```

- from a container you can also access network interface from other container:
```
--net=container:<id|name>
```

# 2. Demo (pid mode)
```
docker image pull dnaprawa/htop

docker container run -it --rm --pid=host dnaprawa/htop
docker container run -p 9001:80 --name htop-nginx nginx
docker container run -it --rm --pid=container:htop-nginx dnaprawa/htop
```

# 3. Demo (network mode)
```
docker container run -it --net container:nginx docker nicolaka/netshoot ngrep -d eth0 -x -q
```