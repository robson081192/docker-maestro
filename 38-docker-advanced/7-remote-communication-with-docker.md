# 1. Intro
- Na lokalnej maszynie generujemy pare kluczy - prywatny i publiczny
```
    ssh-keygen -t rsa -b 4096
```
Klucza prywatnego nie dajemy nikomu!!!
Kopiujemy klucz publiczny z lokalnej maszyny na serwer do lokalizacji
```~/.ssh/authorized_keys```

Uwaga: Jeśli katalog ```.ssh``` lub plik ```authorized_keys``` nie istnieje - tworzymy go, a następnie dodajemy uprawnienia:

```
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

# 2. Test połączenia
```
ssh uzytkownik@adres_ip_serwera
yes
```

# 3. Tworzenie docker context'u
```
docker context ls
docker context create <NAZWA> --docker "host=ssh://uzytkownik@adres_ip_serwera"
docker --context <NAZWA> <DOWOLNE_POLECENIE>
```

# 4. Zmiana domyślnego kontekstu
```
docker context use <NAZWA>
```