# 1. Intro

- achived using namespaces concept

Namespace types:
- ProcessId (pid)
- Network (net)
- Filesystem/mount (mnt)
- Inter-proc comms (ipc)
- UTS (uts)
- User (user)

Control Groups (cgroups)
- kluczowy mechanizm wbudowany w jądro linkuxa
- kontrolują dostęp i przydział procesów do zasobów - CPU, pamięci, dysku (I/O)
- grupują procesy
- nadaja priorytety
- odpowiadają za limitowanie przydziału zasobów CPU, pamięci, dysku (I/O)

## 1.1. PID namespace
- Process trees are separated from each other
- During docker run Docker is creating a namespace collection in the background
- This is the first and simpliest for of isolation
- Can be disabled using:
```>
docker run --pid=host
```

## 1.2. Network namespace
- Each container has it's own network interface (ip tables, routing tables, MAC address)
- by default containers don't have access to networks of other containers
- It's possible to controll that by using port publishing on host machine.

## 1.3. Mount namespace
- first namespace introduced in 2002
- a container has access only to the mounted files
- a container does not see files or directories mounted to other containers

## 1.4. User namespace
- containers may have different user ids (uid) and also different group ids (gid)
- user 500 in container A can be mapped to user 1500 on host
- user 500 in container B can by mapped to user 2500 on host

example:
root in container can eb mapped as readonly user on host

## 1.5. Inter-proc comms (ipc)
- containers (and also different processes in containers) may have access to the same memory on host machine.

## 1.6. UTS (uts) 
- each container may have assigned a different host name.