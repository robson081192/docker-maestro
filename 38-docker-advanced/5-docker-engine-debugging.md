# 1. Intro
## 1.1. Enable debug mode
- Edit file ```/etc/docker/daemon.json```
```
{
    "debug": true
}
```

- next after saving this file use command:
```
sudo kill -SIGHUP $(pidof dockerd)
```

- check your changes with:
```
docker info
```

- Debug Mode: true
- run a container for debugging purposes:
```
journalctl --no-pager -u docker.service --since "1 minute ago"
```
 
