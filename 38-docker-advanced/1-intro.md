# 1. Intro

# 2. Docker Architecture
- Docker For Linux and Linux Containers
- It's good to know, how docker works from the internals perspective
- It will be usefull in future modules (security, debugging, troubleshooting)

## 2.1. OCI - Open Container Initiative
- it's the standard, that describes, how create images and runtime environments for containers
- created 22.06.2015 by Docker and CoreOS
- example:
```>
docker run example.com/org/app:v1.0.0
VS
rkt run example.com/org/app:v1.0.0
```
- more resources on opencontainers.org

# 3. Docker engine configuration
- the docker configuration file is stored under:
```>
# Linux
/etc/docker/daemon.json (if the file does not exist you have to create it)

# Windows
Settings -> Docker Engine

# MacOS
Preferences -> Daemon -> Advanced
```
- you can change security settings, debugg issues and other stuff
- in most cases we use the config file for docker registry manipulation, debugging, security, logging.
