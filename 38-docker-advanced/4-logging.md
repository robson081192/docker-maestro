# 1.Intro
## 1.1. Logging drivers
- per Docker Engine ```/etc/docker/deamon.json```
```
"log-driver": "journald"
```

- per container
```
docker run --log-driver=journald
```

IMPORTANT: you can not use more than one logging driver at a time. Docker Community has also some limitations (smaller amount of drivers and it's not possible to send logs to the cloud).

# 2. Demo
Display current logging driver for docker engine:
```
docker info --format '{{.LoggingDriver}}'
```