# 1. Intro
```>
$ENV:WP_PORT=9091
docker compose -p dev up  -d

$ENV:WP_PORT=9092
docker compose -p test up -d

$ENV:WP_PORT=9093
docker compose -p prod up -d

docker compose -p dev down
docker compose -p test down
docker compose -p prod down
```