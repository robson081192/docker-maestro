# 1. Intro
## 1.1. Portainer
- Manage your docker environment (create, delete and monitor containers)
- Instalation == run a container
- cross platform


```>
docker volume create portainer_data

docker container run -d -p 9000:9000 -p 8000:8000 --name portainer -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data --restart always portainer/portainer
```

## 1.2. Dockstation - desktop application
- smaller amount of features than portainer
- common installation process (installation wizard)
- cross platform
- dockstation.io