# 1. Intro
- you can share environment variables using .env files
- the content of those files will be inserted into docker-compose.yml file
- the .env file has nothing to do with the ENV secion in the Dockerfile!!!
- host environment variables have a higher priority than definitions in .env file (in case of matching names on host and .env file)
- terminal environment variables have the highest priority (they override .env file aswell as host environment variables)
# 2. Example
```>
DB_PASSWORD=test123 # in .env file is accessible as
${DB_PASSWORD} in docker-compose.yml file
```
# 3. Create environment variable in PowerShell
```>
$env:DB_NAME="SomeNameForTheDatabase"
```