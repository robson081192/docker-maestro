# 1. Intro
## 1.1. Dry
- running as container
- can be used as monitoring tool
- monitoring and management of docker containers, images, volumes and networks
- docker swarm support
- can be used on single docker host or also on Docker Swarm cluster

```>
docker container run --rm -it -v /var/run/docker.sock:/var/run/docker.sock moncho/dry
```

## 1.2. Lazydocker
- console application
- manual installation
- like Dry
- docker image layer analysis
- container logs and stats
- containers, volumes, images
- cross platform
- https://github.com/jesseduffield/lazydocker