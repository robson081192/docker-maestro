﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace DotNetCoreDemo.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EnvironmentController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;

        public EnvironmentController(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var envData = new
            {
                _environment.ApplicationName,
                _environment.EnvironmentName
            };

            return Ok(envData);
        }
    }
}
