# 1. Host network driver
- only available on linux containers
- container adapter is not being separated from host adapter
- port publish not available (if a container exposes port 80, then after running it, it will still be available on port 80 - no forwarding possible).
- the host network adapter is shared with containers
- no virtual networks created

```>
docker run -d --network host --name db postgres
docker run -d --network host --name web mywebapp
```

# 2. Run wordpres on host network adapter
```>
docker run -d --name db -e MYSQL_DATABASE=exampledb \ 
    -e MYSQL_USER=exampleuser -e MYSQL_PASSWORD=examplepass \
    -e MYSQL_RANDOM_ROOT_PASSWORD=1 --network=host mysql:5.7 

docker run -d --name wp -e WORDPRESS_DB_HOST=127.0.0.1:3306 \ 
    -e WORDPRESS_DB_USER=exampleuser -e WORDPRESS_DB_PASSWORD=examplepass \
    -e WORDPRESS_DB_NAME=exampledb --network=host wordpress:latest
```