## 1. Build dockerfile
```>
nerdctl image build -t myalpine .
```

## 2. Build dockerfile with specific version
```>
nerdctl image build -t myalpine:1.0.0 .
```

## 3. Build dockerfile with specific version and custom repository
```>
nerdctl image build -t robwinkler/myalpine:1.0.0 .
```