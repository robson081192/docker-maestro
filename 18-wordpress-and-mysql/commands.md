# 1. Create wordpress network
```>
docker network create -d bridge wp
```

# 2. Create postgress database container
```>
docker container run -d -p 8090:3306 --name db -e MYSQL_DATABASE=exampledb -e MYSQL_USER=exampleuser -e MYSQL_PASSWORD=examplepassword -e MYSQL_RANDOM_ROOT_PASSWORD=1 --network wp mysql:5.7
```

# 3. Create wp application container
```>
docker run -d -p 8080:80 -e WORDPRESS_DB_HOST=db:3306 -e WORDPRESS_DB_USER=exampleuser -e WORDPRESS_DB_PASSWORD=examplepassword -e WORDPRESS_DB_NAME=exampledb --network wp wordpress:latest
```
