# 1. Introduction
- configure dependencies between containers
- store it inside an .yml file
- you can start a whole project using a single command
- each docker compose file has a version (1, 2, 2.1, 3, ..., 3.8)
- the build containers can communicate with each other (inside of .yml file you can define networks).
- docker-compose is compativle with docker Swarm
- default name for docker compose file is docker-compose.yml, but you can specify explicite the file name using
```>
docker-compose -f docker-compose.dev.yml up
```

# 2. Docker-compose draft

```>
version: '3.8'

services: 
    servicename: # MANDATORY specified by you service name
        image: # OPTIONAL when you are using build section
        container_name: # OPTIONAL = specify the container name
        command: # OPTIONAL - with this section you can override the CMD statement from Dockerfile
        ports: # OPTIONAL - specify the port publishing
        environment: # OPTIONAL - define the environment variables, For example: "-e MY_SQL_PASSWORD=1"
        volumes: # OPTIONAL: -v in docker container run statement
    servicename-2:
        image: # OPTIONAL when you are using build section
        restart: # OPTIONAL - restart policy

volumes: # OPTIONAL

networks: # OPTIONAL
```

# 3. Commands
```>
docker compose up

docker compose down

docker compose down -v
```