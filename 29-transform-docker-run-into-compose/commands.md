# 1. Introduction
PROBLEM: you have prepared multiple docker run statements that are running your infrastructure, and now you would like to convert them into a single docker-compose.yml file.

SOLUTION: install the tool composerize:
```>
node --version
npm install composerize -g
```

# 2. Examples
```>
composerize docker run -p 80:80 --restart always --name composerize-nginx nginx:1.17
```

```>
composerize docker run -d -p 3306:3306 --name db -e MYSQL_DATABASE=exampledb -e MYSQL_USER=exampleuser -e MYSQL_PASSWORD=examplepass -e MYSQL_RANDOM_ROOT_PASSWORD=1 --network=wp --restart=always mysql:5.7
```

```>
composerize docker run -d -p 8080:80 -e WORDPRESS_DB_HOST=db:3306 -e WORDPRESS_DB_USER=exampleuser -e WORDPRESS_DB_PASSWORD=examplepass -e WORDPRESS_DB_NAME=exampledb --network=wp --restart=always wordpress:latest
```

```>
composerize docker run -d -p 8080:80 -e WORDPRESS_DB_HOST=db:3306 -e WORDPRESS_DB_USER=exampleuser -e WORDPRESS_DB_PASSWORD=examplepass -e WORDPRESS_DB_NAME=exampledb --network=wp --restart=always wordpress:latest > docker-compose.yml
```