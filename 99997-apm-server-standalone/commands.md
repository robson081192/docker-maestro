## 1. Download apm-server image
```>
nerdctl pull docker.elastic.co/apm/apm-server:7.16.3
```

## 2. Run the container with volume-mounted config
```>
nerdctl run -d -p 127.0.0.1:8200:8200 --name=apm-server --user=apm-server --volume="./apm-server/config/apm-server.yml:/usr/share/apm-server/apm-server.yml:ro" --network=elk docker.elastic.co/apm/apm-server:7.16.3 --strict.perms=false -e
```