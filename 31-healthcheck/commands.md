# 1. Introduction
- for a production system application monitoring and stats analysis is crucial
- the same rule applies for processes that are running in containers
- an container should be empheral (possible to be replaced by another container at any time, stateless)
- it should be possible to monitor a container

# 2. Monitoring layers
- CPU
- RAM
- I/O
- Network
- whole infrastructure (docker engine, docker swarm, kubernetes)
- application monitoring (time of response, logs, request throughput)

# 3. Docker container monitoring - simpliest solution
```>
docker stats
```

# 4. Demo
```>
docker container run -d --name apache1 httpd

docker container run -d --name apache2 httpd

docker container run -d --name redis1 redis

docker container run -d --name redis2 redis

docker ps

docker container ls

docker stats
docker stats --no-stream

docker container rm $(docker ps -aq) --force
```

# 5. DEMO - Simple monitoring
- using the cAdvisor tool
- it's open-source
- it runs in the browser
- realtime resources consumption
- networks usage statistics
- historical data

First run the containers
```>
docker container run -d --name apache1 httpd

docker container run -d --name apache2 --memory="100m" httpd

docker container run -d --name redis1 redis

docker container run -d --name redis2 --memory="30m" redis
```

Run the monitoring container
```>
docker run --volume=/:/rootfs:ro --volume=/var/run:/var/run:ro --volume=/sys:/sys:ro --volume=/var/lib/docker/:/var/lib/docker:ro --volume=/dev/disk/:/dev/disk:ro --publish=8080:8080 --detach=true --name=cadvisor gcr.io/google-containers/cadvisor:latest
```

# 6. Demo - Advanced monitoring
