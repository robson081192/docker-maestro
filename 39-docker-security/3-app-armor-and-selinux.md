# 1. Intro - what is AppArmor
- Linux Security Module (LMS)
- secures the operating system using profiles for applications and containers
- default security module for following distros: Ubuntu, Debian (ver. >= 10), OpenSUSE
- It makes it possible to control file access, network access and task execution (chown, setuid etc)

# 2. How can we increase docker container security using AppArmor?
- create a new profile for each container (not per docker deamon)
- by default each container is started using docker-default profile
```
docker run --rm -it --security-opt apparmor=docker-default hello-world sh
THATS IS EQUAL TO:
docker run --rm -it hello-world sh
```

- you can start a container without any profile (NOT RECOMMENDED)
```
--security-opt apparmor=unconfined
```

# 3. SELinux
- AppArmor for RHEL (CentOS and RedHat)

# 4. Demo
```
docker container run -dit --name apparmor1 alpine sh

apparmor_status
```