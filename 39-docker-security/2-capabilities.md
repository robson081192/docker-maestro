# 1. Intro
- it's a linux mechanism
- you can change (decrease/increase) root permissions using capabilities
- example: CAP_CHOWN - it allows you to changes the owner of a file/directory for an user or group
- by default docker removes all capabilities and adds only those, that are required by docker. You can change the list of capabilities afterwards:
```
docker run --cap-drop <CAP_TYPE> <image>
docker run --cap-drop ALL <image>
docker run --cap-add <CAP_TYPE> <image>
```

# 2. Demo
```
docker container run --rm -it alpine chown nobody /

docker container run --rm -it --cap-drop ALL --cap-add CHOWN alpine chown nobody /

docker container run --rm -it --cap-drop CHOWN alpine chown nobody /

docker container run --rm -it --cap-add CHOWN -u nobody alpine chown nobody /
```