# 1. Introduction - secure the docker engine API accessible over https

Requirements:
- cerfiticates (best from trusted CA)

File: ```/etc/docker/daemon.json```:
```
{
  "tls": true,
  "tlscacert": "/etc/docker/ssl/ca.pem",
  "tlscert": "/etc/docker/ssl/server-cert.pem",
  "tlskey": "/etc/docker/ssl/server-key.pem",
  "hosts":["unix:///var/run/docker.sock","tcp://0.0.0.0:2376"]
}
```

On client:
export DOCKER_TLS_VERIFY=1
export DOCKER_HOST=tcp://dockermayestro1.northeurope.cloudapp.azure.com:2376
docker container ls

# 2. How get rid of root account on container
run in root mode(default)
```
docker container run -itd --name ubuntu-defaultuser -v /:/hacking ubuntu /bin/bash

docker container top ubuntu-defaultuser
```

run in non-root mode
```
docker container run -itd -user 998 --name ubuntu-customuser -v /:/hacking ubuntu /bin/bash

docker container top ubuntu-customuser
```

# 3. User re-mapping
Modify file ```/etc/docker/daemon.json```
```
"userns-remap": "default" 
```

Then restart docker engine with:
```
sudo systemctl restart docker
```

Run a container:
```
docker run -itd --name ubuntu-remmaped ubuntu bash
docker exec -it ubuntu-remmaped ps aux
ps aux | grep bash
```