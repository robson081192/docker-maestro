# Pull ngingx image from docker hub
```>
nerdctl image pull nginx:latest
```

# Run nginx DETACHED and forward container port (80) to host port (8080) with name mynginx
```>
nerdctl container run -d -p 8080:80 --name mynginx nginx:latest
```

# Pull postgress image
```>
nerdctl image pull postgres:latest
```

# Run postgres image and forward container port 5432 to host port 5444
```>
nerdctl container run -d -e POSTGRES_USER=user1 -e POSTGRES_PASSWORD=pass123 -p 5444:5432 postgres:latest
```

# Create and then run the container
```>
docker container create -p 8081:80 --name nginx2 nginx:latest
docker container ls 
docker container start nginx2
curl localhost:8081
```