## 1. Pull base image
```>
docker image pull alpine:3.14
```

## 2. Create new tag
```>
docker image tag alpine:3.14 robwinkler/alpine:3.14
```

## 3. Login to dockerhub
```>
docker login --username robwinkler
```

## 4. Push the image to dockerhub
```>
nerdctl image push robwinkler/alpine:3.14
```

## 5. Remove all images
```>
nerdctl image rm $(nerdctl image ls -aq)
```

## 6. Pull the image from dockerhub
```>
nerdctl image pull robwinkler/alpine:3.14
```

## 7. Run the image in interactive mode
```>
nerdctl container run -it robwinkler/alpine:3.14 sh
```