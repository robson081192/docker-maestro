## 1. Display image layers
```>
nerdctl image inspect robwinkler/dotnetcoredemo-web:1.0.0
```

## 2. Display image manifest
```>
nerdctl manifest inspect robwinkler/dotnetcoredemo-web:1.0.0
```

# 3. Display image history
```>
docker image history nginx
```

## 4. Tool for image inspection
```>
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest robwinkler/aspnetcoredemo-web:1.0.0
```