# 1. Intro
- hardware host monitoring
- container monitoring - cAdvisor
- storage of metrics
- live visualization
- notifications

# 2. Monitoring stack - description
- Prometheus Node Exporter - host hardware monitoring
- cAdvisor - container monitoring
- Prometheus - metrics storage and data provider for Grafana
- Prometheus Alert Manager - notification management
- Grafana - dashboard (data visualization)

# 3. Demo
```>
ADMIN_USER=admin ADMIN_PASSWORD=admin docker compose up -d
```