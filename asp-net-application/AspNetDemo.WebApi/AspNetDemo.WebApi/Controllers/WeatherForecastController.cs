﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetDemo.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("{guidCount}")]
        public async Task<IEnumerable<Guid>> Get(int guidCount)
        {
            var rng = new Random();
            var tasks = new List<Task<Guid>>();
            for (int i = 0; i < guidCount; i++)
            {
                var allocateMemory = rng.Next(1, 10) * 1024 * 1024;
                var task = GenerateGuid(allocateMemory);
                tasks.Add(task);
            }

            var guids = await Task.WhenAll(tasks);
            return guids.ToList();
        }

        private async Task<Guid> GenerateGuid(long allocatedMemory)
        {
            var allocatedBytes = new byte[allocatedMemory];
            await Task.Delay(500);
            return Guid.NewGuid();
        }
    }
}
