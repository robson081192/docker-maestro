using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AspNetDemo.WebApi.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace AspNetDemo.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var elkIndexFormat =
                $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}";
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                //.Enrich.WithProperty("Version", "1.2.3")
                .Enrich.With<ThreadIdEnricher>()
                /*.WriteTo.Http("localhost:5000")*/ // Logstash endpoint
                .WriteTo.Elasticsearch("http://localhost:9200", elkIndexFormat, autoRegisterTemplate: true)
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:HH:mm} [{Version}] [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
                .WriteTo.File("Logs/log.txt", LogEventLevel.Verbose)
                .CreateLogger();

            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
