# Commands
## 1. Create first container
```>
docker container run hello-world
```
OR
```>
nerdctl container run hello-world
```


## 2. Create ubuntu container
```>
nerdctl container run ubuntu:latest
```

## 3. Display all containers (also those that are not running)
```>
nerdctl container ls -a
```

## 4. Run container DETACHED and INTERACTIVE with bash as running program
```>
docker container run -it -d ubuntu:latest bash
```
IMPORTANT: This is not working with nerdctl!!!
```>
nerdctl container run -it ubuntu:latest bash
```