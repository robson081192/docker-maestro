# 1. Network type - bridge
- default network type for containers - if you don't specify a network, then by default a 'default' network is attached of type bridge
- the simplest and most popular one
- used for communication between containers that are running on the same host
- access granted by port publishing (-p 8082:80)
- containers using default network are communicating only by IP Address
- poor isolation level
- to detach a container from the default network you have to stop it first and the run it with different network settings

```>
docker network create -d bridge mybridge

docker run -d --net mybridge --name mypostgres postgres:9.6

docker run -d --net mybridge -e DB=db -p 8080:3000 --name web mywebapi:1.0.0
```

# 2. Demo
```>
docker image pull ubuntu

docker network create -d bridge ubuntu-bridge

nerdctl container run -it --name myubuntu1  --net ubuntu-bridge ubuntu bash

nerdctl container run -it --name myubuntu2  --net ubuntu-bridge ubuntu bash

nerdctl network ls

nerdctl network inspect ubuntu-bridge
```

install tooling on ubuntu machine
```>
apt-get update & apt-get install -y ip-utils
apt-get update & apt-get install -y net-tools

ifconfig
ping <ip_address>
```

connect/disconnect to/from network
```>
docker network connect mybridge ubuntu1

docker network disconnect mybridge ubuntu1
```