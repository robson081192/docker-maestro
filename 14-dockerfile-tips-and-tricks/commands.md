# 1. COPY vs ADD
- copy is recommended
- single use case for ADD is to add an archive to the image and extract it in a single operation
- ADD can be dangerous. For example: ADD http://example.com/big.tar.xz /home
- instead of that use RUN curl -SL http://example.com/big.tar.xz | tar -xJC

# 2. ENTRYPOINT vs CMD
Recommended:
```>
ENTRYPOINT ["executable", "param1", "param2"]
CMD ["executable", "param1", "param2"]
```

- IMPORTANT: ENTRYPOINT can not be overriden like CMD. All paraemters of ENTRYPOINT are being executed. You can override CMD with last parameter (after image in docker run statement).

# 3. ARG vs ENV
- ARG is available already in Dockerfile
- ENV available in container. We can also use it, when the image is ready (build).

# FROM SCRATCH
- used for base images
