# 1. Intro
Problem description:
- you would like to have different configuration for different environments.
- for dev purposes you would like to open ports for the database (make it accessible from host). In production those ports should be closed
- for prod environment you would like to run the containers automatically during restart of docker engine (startup)

Solution:
- you have one basic docker-compose.yml - common project settings
- he files docker-compose.override.yml and docker-compose.prod.yml contain additional configuration settings
- run in following order:
```>
docker compose -f docker-compose.yml -f docker-compose.dev.yml up

or

docker compose -f docker-compose.yml -f docker-compose.prod.yml up
```