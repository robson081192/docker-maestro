# 1. Intro
- more than one FROM instruction
- does not matter where the image is being build (locally or CI/CD) - it ensures consistency
- the final image is lightweight and does not contain any temporary files - that were generated during build of earlier stages

# 2. Exercises
## 2.1. Build production image
```>
docker image build -t myreact:latest .
```

## 2.1. Build production image
```>
docker image build -t myreact:latest .

docker container run -d -p 8082:80 --name react-prod  myreact:latest
```

## 2.2. Build only development stage
```>
docker image build --target development --build-arg  NODE_ENV=development -t react-dev:latest .

docker run -d -p 3000:3000 --name react-dev react-dev:latest

docker container ls

docker image inspect react-dev:latest
```