## 1.Run apache server in version 2.4
```>
docker pull httpd:2.4
docker container run --name myapache --publish 8000:80 --detach --rm httpd:2.4
```

## 2. Run mysql server in version 5.7
```>
docker pull mysql:5.7
docker container run --name db -p 3306:3306 -d -e MYSQL_ROOT_PASSWORD=mysecret-password --restart always mysql:5.7
```