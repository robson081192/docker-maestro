# 1. Intro
PROBLEM: seperate different project parts
- seperate using dedicated docker-compose.yml files
- seperate using different networks
    
Example:
- you have a collection of infrastructure containers and an application
- you create a compose file for the whole infrastructure and a seperate compose file for the app
- to make it possible to communicate with the infrastructure, the app need to connect with the infrastructure network. Remember by default the compose file defines the network name. So creating two seperate compose files will not resolve that issue by default.
- YOU NEED AN EXTERNAL NETWORK

# 2. Commands
```>
docker network create -d bridge myproject-external-network
docker network ls
docker compose -f docker-compose.vault.yml up -d
docker container run -it --net myproject-external-network ubuntu bash
apt-get update & apt-get install -y iputils-ping
ping vault
```