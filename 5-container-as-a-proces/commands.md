## 1. Display container process information
```>
nerdctl container run -itd --name myubuntu ubuntu:latest bash

docker container top myubuntu
or
nerdctl top myubuntu
```

## 2. Execute command in container
```>
docker container exec -it mymongo bash

### and the inside mongo container bash run following command:
ps -aux
```

## 3. Identify if you are inside a container or outside
```>
# run this inside of a container (for example using bash)
cat /proc/1/cgroup
```