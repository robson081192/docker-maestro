## 1. Create nginx container
```>
nerdctl image pull nginx:1.21.6

nerdctl container run --name mynginx -p 8080:80 -d nginx:1.21.6
```

## 2. Copy index.html file from container to host
```>
// display file content:
nerdctl container exec mynginx cat /usr/share/nginx/html/index.html

// copy file
docker container cp mynginx:/usr/share/nginx/html/index.html index.html
```

## 3. Modify index.html file that has been copied to localhost

## 4. Create new image with modified index.html file
```>
nerdctl image build -t robwinkler/mynginx:1.21.6 .

nerdctl container run -p 8081:80 -d --name mynginx2 robwinkler/mynginx:1.21.6
```

## 5. Push that image to dockerhub
```>

```