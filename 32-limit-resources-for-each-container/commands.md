# 1. Intro
- by default a container is not limited - resources are assigned by ControlGroups
- we can manually adjust the resources, that can be assigned for a container
- set max. available RAM
```>
docker container run -d -p 8081:80 --memory="256m" nginx
docker container run --memory="50m" --rm busybox free -m
```

- perform stress test
```>
docker run --memory 50m --rm -it \
  progrium/stress --vm 1 --vm-bytes 62914560 --timeout 3s

docker run --memory 50m --memory-swap 50m --rm -it \
  progrium/stress --vm 1 --vm-bytes 62914560 --timeout 3s
```

- set max. available CPUs
```>
docker run -it --cpus=".5" ubuntu /bin/bash
```
- limitations
```>
--cpus="0.6" # 60% of CPU available (all cores)
--cpuset-cpus="1,3" # only core number 2 and 4 available (0-based index)
--memory="512M" # set max memory limit. min is 4MB
```

- what happens during Out Of Memory Exceptions?
When you don't specify a memory limit it's possible that a crucial system process will be closed. It's also possible that the whole host will be shut down. It's better to run a container with memory limitations, because in the worst case scenario the a container will be shut down or the docker engine (the host machine will survive). So set always a memory limit especially in case of containers where the memory usage can exceed the hosts available memory.

# 2. Demo RAM
```>
docker container run --memory="50m" --rm busybox free -m

docker container run --memory 50m --rm -it progrium/stress --vm 1 --vm-bytes 62914560 timeout 3s

docker container run --memory 50m --memory-swap 50m --rm -it progrium/stress --vm 1 --vm-bytes 62914560 --timeout 3s
```

# 3. Demo CPU
```>
docker container run -it --cpus="0.5" ubuntu /bin/bash
```